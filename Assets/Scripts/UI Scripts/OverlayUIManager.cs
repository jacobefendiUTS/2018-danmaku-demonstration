﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class OverlayUIManager : MonoBehaviour {

    public static OverlayUIManager overlayUIManager;

    // variables for audio
    public AudioSource audioSource;
    public AudioClip[] audioClips;

    // variables for pause menu
    public static bool paused = false;
    public GameObject pauseMenuUI;
    public Button[] pauseButtons;

    // variables for continue menu
    public GameObject continueMenuUI;
    public Text continuesLeftText;
    public Button[] continueButtons;

    int menuButtonIndex = 0;

    int initialBossHealth; // the starting health of the enemy
    EnemyManager enemyManager; // script that contains properties of enemy
    public Slider bossHealthSlider; // the slider that shows the enemy health

    // variables for player lives
    public GameObject playerLivesParent;
    public Image[] playerLives; // array to contain player life images
    public Image currentLife; // the current life player is one
    int numberOfLives; // the current number of lives 

    public Text bossNameText; // the text that shows the enemy name
    public Text bossLifeText; // the text that shows the boss' life
    public Text playerScoreText; // the text that shows the player's score
    public Text playerPowerText; // the text that shows the player's power

    void Awake()
    {
        overlayUIManager = this;

    }

    void Start()
    {
        // get the enemy
        GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
        // get enemy manager script from enemy
        enemyManager = enemy.GetComponent<EnemyManager>();

        // set slider and text values based on enemy properties
        initialBossHealth = enemyManager.initialHealth;
        bossNameText.text = enemyManager.GetName();
        bossLifeText.text = initialBossHealth + "/" + initialBossHealth;

        // set the player score and power points text
        playerScoreText.text = "Score: " + PlayerManager.playerManager.GetScore();
        playerPowerText.text = "Power: " + PlayerManager.playerManager.GetPowerPoints() + "/" + PlayerManager.playerManager.GetMaxPowerPoints();

        // set the player lives
        numberOfLives = playerLivesParent.transform.childCount;
        playerLives = new Image[numberOfLives];
        for (int i = 0; i < playerLives.Length; i++)
        {
            playerLives[i] = playerLivesParent.transform.GetChild(i).GetComponent<Image>();
            playerLives[i].enabled = true;
        }
        currentLife = playerLives[playerLives.Length-1];
    }

    void Update()
    {
        // pause/resume game with escape button
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused && pauseMenuUI.activeSelf) { Resume(); }
            else { OpenPauseMenu(); }
        }

        // handle pause/continue menu buttons if game is paused
        if (paused)
        {
            if (pauseMenuUI.activeSelf)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (menuButtonIndex <= 0) { menuButtonIndex = pauseButtons.Length - 1; }
                    else { menuButtonIndex--; }
                    audioSource.clip = audioClips[0];
                    audioSource.Play();
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (menuButtonIndex >= pauseButtons.Length - 1) { menuButtonIndex = 0; }
                    else { menuButtonIndex++; }
                    audioSource.clip = audioClips[0];
                    audioSource.Play();
                }
                pauseButtons[menuButtonIndex].Select();
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    pauseButtons[menuButtonIndex].onClick.Invoke();
                    audioSource.clip = audioClips[1];
                    audioSource.Play();
                }
            }
            else if (continueMenuUI.activeSelf)
            {
                if (PlayerManager.playerManager.GetContinues() <= 0) { continueButtons[0].interactable = false; }
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (menuButtonIndex <= 0) { menuButtonIndex = continueButtons.Length - 1; }
                    else { menuButtonIndex--; }
                    audioSource.clip = audioClips[0];
                    audioSource.Play();
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (menuButtonIndex >= continueButtons.Length - 1) { menuButtonIndex = 0; }
                    else { menuButtonIndex++; }
                    audioSource.clip = audioClips[0];
                    audioSource.Play();
                }
                continueButtons[menuButtonIndex].Select();
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    continueButtons[menuButtonIndex].onClick.Invoke();
                    audioSource.clip = audioClips[1];
                    audioSource.Play();
                }
            }
        }

        // update slider to display current enemy health
        bossHealthSlider.value = (float)enemyManager.GetHealth() / initialBossHealth;
        bossLifeText.text = enemyManager.GetHealth() + "/" + initialBossHealth;

        // update player health, score and power points
        playerScoreText.text = "Score: " + PlayerManager.playerManager.GetScore();
        playerPowerText.text = "Power: " + PlayerManager.playerManager.GetPowerPoints() + "/" + PlayerManager.playerManager.GetMaxPowerPoints();

        // update player lives
        for (int i = 0; i < playerLives.Length; i++) { playerLives[i].enabled = false; }
        for (int i = 0; i < PlayerManager.playerManager.GetLives(); i++) { playerLives[i].enabled = true; }

        numberOfLives = PlayerManager.playerManager.GetLives();
        if (numberOfLives > 0) { currentLife = playerLives[PlayerManager.playerManager.GetLives() - 1]; }
        currentLife.fillAmount = (float)PlayerManager.playerManager.GetHealth() / PlayerManager.playerManager.initialHealth;
    }

    // function to open pause menu
    void OpenPauseMenu()
    {
        // activate the pause menu
        EventSystem.current.SetSelectedGameObject(null);
        pauseMenuUI.SetActive(true);
        menuButtonIndex = 0;

        // pause the game
        Time.timeScale = 0;
        paused = true;
    }

    // function to open continue menu
    public void OpenContinueMenu()
    {
        // activate the continue menu
        EventSystem.current.SetSelectedGameObject(null);
        continueMenuUI.SetActive(true);
        continuesLeftText.text = "Continues Left: " + PlayerManager.playerManager.GetContinues();

        // pause the game
        Time.timeScale = 0;
        paused = true;
    }

    // function to activate player continue
    public void PlayerContinue()
    {
        // reset player and subtract continues
        PlayerManager.playerManager.ResetPlayer();
        PlayerManager.playerManager.SubtractContinues();

        // reactivate life GUI
        for (int i = 0; i < PlayerManager.playerManager.GetLives(); i++)
        {
            playerLives[i].enabled = true;
            playerLives[i].fillAmount = 1;
        }

        // resume the game
        Resume();
    }

    // function to resume the game
    public void Resume()
    {
        // deactivate pause/continue menu
        EventSystem.current.SetSelectedGameObject(null);
        if (pauseMenuUI.activeSelf) { pauseMenuUI.SetActive(false); }
        if (continueMenuUI.activeSelf) { continueMenuUI.SetActive(false); }

        // unpause the game
        Time.timeScale = 1;
        paused = false;
    }

    // functions to return to main menu
    public void Quit() { StartCoroutine(ReturnToMainMenu()); }

    IEnumerator ReturnToMainMenu()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(0);
        while (!operation.isDone)
        {
            yield return null;
        }
    }

    // function to resume game when level is loaded
    void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        paused = false;
        Time.timeScale = 1;
    }

    void OnEnable() { SceneManager.sceneLoaded += OnLevelLoaded; }
    void OnDisable() { SceneManager.sceneLoaded -= OnLevelLoaded; }
}
