﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour {

    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public Button[] menuButtons;
    int menuButtonIndex = 0;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (menuButtonIndex <= 0) { menuButtonIndex = menuButtons.Length - 1; }
            else { menuButtonIndex--; }
            audioSource.clip = audioClips[0];
            audioSource.Play();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (menuButtonIndex >= menuButtons.Length - 1) { menuButtonIndex = 0; }
            else { menuButtonIndex++; }
            audioSource.clip = audioClips[0];
            audioSource.Play();
        }
        menuButtons[menuButtonIndex].Select();
        if (Input.GetKeyDown(KeyCode.Z))
        {
            menuButtons[menuButtonIndex].onClick.Invoke();
            audioSource.clip = audioClips[1];
            audioSource.Play();
        }
    }

    // functions to load scene
    public void LoadLevel(int sceneIndex) { StartCoroutine(LoadSceneAsync(sceneIndex)); }
    IEnumerator LoadSceneAsync(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while (!operation.isDone)
        {
            yield return null;
        }
    }

    // function to exit game
    public void Exit() { Application.Quit(); }

}
