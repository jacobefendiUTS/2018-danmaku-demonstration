﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpCollision : MonoBehaviour {

    public int speed;
    public bool weaponUpgrade;

    // variables for homing in on player
    public Transform target;
    public bool homing = false;
    float distance;
    public float maxDistance;
    Vector2 direction;

    void Start()
    {
        // find the target
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        distance = Vector3.Distance(target.transform.position, transform.position);
        if (distance <= maxDistance && !homing)
        {
            homing = true;
            speed = speed * 2;
        }

        if (homing && target.gameObject.activeSelf)
        {
            direction = (target.transform.position - transform.position).normalized;
            transform.Translate(direction * speed * Time.deltaTime);
        }
        else { transform.Translate(Vector2.down * speed * Time.deltaTime); }
    }

    // function to handle collisions with player
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // get current and maximum upgrade points of player
            int powerPoints = PlayerManager.playerManager.GetPowerPoints();
            int maxPowerPoints = PlayerManager.playerManager.GetMaxPowerPoints();

            // add upgrade points if powerup is a weapon upgrade powerup
            // add score if powerup is a score powerup
            if (weaponUpgrade && powerPoints < maxPowerPoints) { PlayerManager.playerManager.AddPowerPoints(1); }
            else { PlayerManager.playerManager.AddScore(10); }

            // play audio
            PlayerManager.playerManager.PlaySound(3);

            // deactivate powerup  object
            gameObject.SetActive(false);
        }
    }
}
