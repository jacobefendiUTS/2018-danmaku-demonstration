﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script which allows bullets to inflict damage
public class BulletDamage : MonoBehaviour
{
    public int damage; // the amount of damage inflicted by bullet
    public Color impactColour; // colour of impact explosion

    // function to manage collisions
    void OnTriggerEnter2D(Collider2D other)
    {
        // manage player bullet collisions
        if (tag == "PlayerBullet")
        {
            // damage enemies
            if (other.gameObject.CompareTag("Enemy"))
            {
                EnemyManager enemyManager = other.gameObject.GetComponent<EnemyManager>();
                if (enemyManager.GetHealth() > 0)
                {
                    enemyManager.damageEnemy(damage);
                    PlayerManager.playerManager.AddScore(10);
                }

                // create impact explosions and deactivate bullet
                createExplosion();
                gameObject.SetActive(false);
            }

            // deactivate bullet if it hits barrier
            if (other.gameObject.CompareTag("Barrier"))
            {
                createExplosion();
                gameObject.SetActive(false);
            }
        }

        // manage enemy bullet collisions
        if (tag == "EnemyBullet")
        {
            // damage players
            if (other.gameObject.CompareTag("Player") && !PlayerManager.playerManager.GetInvulerable())
            {
                PlayerManager.playerManager.DamagePlayer(damage);
                createExplosion();
                gameObject.SetActive(false); 
            }

            // add score and deactivate if it hits a barrier
            if (other.gameObject.CompareTag("Barrier"))
            {
                PlayerManager.playerManager.AddScore(5);
                gameObject.SetActive(false);
            }
        }

        // deactivate bullets that hit the boundary
        if (other.gameObject.CompareTag("Boundary"))
        {
            gameObject.SetActive(false);
        }
    }

    // function to create impact explosion
    void createExplosion()
    {
        GameObject explosion = ObjectPoolManager.objectPoolManager.GetPooledObject("BulletExplosion");
        explosion.GetComponent<BulletImpact>().SetExplosionColour(impactColour);
        explosion.transform.position = transform.position;
    }

    void OnDisable() { gameObject.tag = "Untagged"; }
}