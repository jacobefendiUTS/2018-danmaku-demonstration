﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLimitedHomingMovement : MonoBehaviour {

    public int speed; // the base speed of the bullet
    public bool homing;
    public GameObject[] targets; // array to contain potential targets
    public GameObject target; // the chosen target;

    Vector3 direction; // the direction bullet is moving in
    public float distance; // the distance from the bullet and the target
    public float maxDistance; // the maximum distance from bullet and target before bullet no longer homes

    // initialise variables here
    void Awake()
    {
        direction = Vector3.forward;
    }

    // initialise variables from other scripts here
    void Start()
    {
        // see if there are any enemies/player
        if (gameObject.CompareTag("PlayerBullet"))
        {
            targets = GameObject.FindGameObjectsWithTag("Enemy");
            if (targets != null) { FindTarget(); }
        }
        else if (gameObject.CompareTag("EnemyBullet"))
        {
            target = GameObject.FindGameObjectWithTag("Player");
            homing = true;
        }
    }

    void Update()
    {
        // if there is no target or homing is disabled, just move forward
        if (target == null || !homing)
        {
            transform.Translate(direction * speed * Time.deltaTime);
            GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 0.0f);
        }
        else if (homing)
        {
            // find distance between bullet and target
            distance = Vector3.Distance(target.transform.position, transform.position);
            // turn off homing
            if (distance <= maxDistance) { homing = false; }
            // if there is a target find the direction it is in and move towards it
            if (gameObject.CompareTag("PlayerBullet"))
            {
                direction = (target.transform.position - transform.position).normalized;
                transform.Translate(direction * speed * Time.deltaTime);
            }
            else
            {
                direction = (transform.position - target.transform.position).normalized;
                transform.Translate(direction * speed * Time.deltaTime);
            }
        }
    }

    // function to find target
    void FindTarget()
    {
        float closestDistance = 1000.0f; // float to contain closest distance
        if (gameObject.CompareTag("PlayerBullet"))
        {
            // find all potential targets
            targets = GameObject.FindGameObjectsWithTag("Enemy");

            // find distances between the targets and player
            for (int i = 0; i < targets.Length; i++)
            {
                float distance = Vector3.Distance(transform.position, targets[i].transform.position);

                // set the target with least distance as target
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    target = targets[i];
                }
            }
        }
        if (target != null) { homing = true; }
    }

    void OnDisable()
    {
        homing = true;
        GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
    }
}
