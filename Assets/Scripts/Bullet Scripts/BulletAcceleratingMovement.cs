﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAcceleratingMovement : MonoBehaviour {

    public int speed; // the base speed of the bullet
    public int delay; // delay before bullet starts accelerating
    public float currentAcceleration; // the current acceleration of the bullet
    public float accelerationFactor; // how much the bullet accelerates by per frame
    public float maxAcceleration; // the maximum acceleration of the bullet

    void Start() { currentAcceleration = 0.0f; }

    void Update()
    {
        if (delay > 0)
        {
            if (!OverlayUIManager.paused) { delay--; }
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }
        else
        {
            // increase the current acceleration 
            if (currentAcceleration < maxAcceleration) { currentAcceleration += accelerationFactor; }
            transform.Translate(Vector2.up * speed * currentAcceleration * Time.deltaTime);
        }
    }

    void OnDisable()
    {
        delay = 50;
        currentAcceleration = 0.0f;
    }
}
