﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletChangeDirectionMovement : MonoBehaviour {

    public float speed; // the base speed of the bullet
    public float currentDecceleration; // the current decceleration of the bullet
    public float deccelerationFactor; // the amount that the bullet accelerates by

    public Vector2 currentDirection; 
    public bool changedDirection;
    public bool moveLeft;
    public int directionalFactor; // the number which determines the next direction of the bullet

    public int delay; // the amount of delay before the bullet starts changing directions

    void Start()
    {
        currentDecceleration = speed;
        currentDirection = Vector2.up;
    }

    void Update()
    {
        if (delay > 0) { delay--; }

        if (delay <= 0)
        {
            if (changedDirection)
            {
                if (currentDecceleration > 0)
                {
                    currentDecceleration -= deccelerationFactor;
                    transform.rotation = Quaternion.LookRotation(currentDirection);
                    GetComponent<Rigidbody>().velocity = currentDirection * speed * currentDecceleration;
                }
                else
                {
                    currentDecceleration = speed;
                    if (moveLeft)
                    {
                        currentDirection = (Vector2)transform.up + Vector2.right;
                        moveLeft = false;
                    }
                    else
                    {
                        currentDirection = (Vector2)transform.up + Vector2.left;
                        moveLeft = true;
                    }
                    transform.Translate(currentDirection * 0);
                }
            }
            else
            {
                directionalFactor = Random.Range(0, 10);
                if (directionalFactor > 5)
                {
                    currentDirection = (Vector2)transform.up + Vector2.right;
                    moveLeft = false;
                }
                else
                {
                    currentDirection = (Vector2)transform.up + Vector2.left;
                    moveLeft = true;
                }
                changedDirection = true;
            }
        }
        else { transform.Translate(currentDirection * speed / 2 * Time.deltaTime); }
    }
}
