﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script for basic (forward) bullet movement
public class BulletBasicMovement : MonoBehaviour {

    public int speed; // the speed of the bullet

    void Update() { transform.Translate(Vector2.up * speed * Time.deltaTime); }
}
