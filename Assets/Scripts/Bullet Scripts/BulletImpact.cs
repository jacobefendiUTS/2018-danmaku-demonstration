﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletImpact : MonoBehaviour {

    // function to disable explosion once animation is done
    void DisableExplosion()
    {
        gameObject.SetActive(false);
    }

    // function to set colour of the explosion
    public void SetExplosionColour(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }
}
