﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHomingMovement : MonoBehaviour {

    public int speed; // the base speed of the bullet
    public GameObject[] targets; // array to contain potential targets
    public GameObject target; // the chosen target;

    // initialise variables with other scripts here
    void Start()
    {
        // see if there are any enemies
        if (gameObject.CompareTag("PlayerBullet"))
        {
            targets = GameObject.FindGameObjectsWithTag("Enemy");
            if (targets != null) { FindTarget(); }
        }
        else if (gameObject.CompareTag("EnemyBullet")) { target = GameObject.FindGameObjectWithTag("Player"); }
    }

    void Update()
    {
        // if there is no target, just move forward
        if (target == null || !target.activeSelf) { transform.Translate(Vector2.up * speed * Time.deltaTime); }
        else
        {
            // if there is a target find the direction it is in and move towards it
            if (gameObject.CompareTag("PlayerBullet"))
            {
                Vector2 direction = (target.transform.position - transform.position).normalized;
                transform.Translate(direction * speed * Time.deltaTime);
            }
            else
            {
                // do move from the reverse direction if it is from an enemy
                Vector2 direction = (transform.position - target.transform.position).normalized;
                transform.Translate(direction * speed * Time.deltaTime);
            }
        }
    }

    // function to find target
    void FindTarget()
    {
        float closestDistance = 1000.0f; // float to contain closest distance
        if (gameObject.CompareTag("PlayerBullet"))
        {
            // find all potential targets
            targets = GameObject.FindGameObjectsWithTag("Enemy"); 

            // find distances between the targets and player
            for (int i = 0; i < targets.Length; i++)
            {
                float distance = Vector2.Distance(transform.position, targets[i].transform.position);

                // set the target with least distance as target
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    target = targets[i];
                }
            }
        }
    }
}
