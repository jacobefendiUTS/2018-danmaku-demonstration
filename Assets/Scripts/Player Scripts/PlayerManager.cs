﻿using System.Collections;
using UnityEngine;

// script to contain player properties and movement controls
public class PlayerManager : MonoBehaviour {

    public static PlayerManager playerManager;

    // variables for death and respawning
    public GameObject deathExplosion; // explosion that is used when player dies
    public bool invulnerable; // boolean for if player is invulnerable
    public int invulnerabilityCooldown; // current cooldown of invulnerability
    public int invulnerabilityCooldownTime; // the cooldown time for invulnerability
    bool lifeSubtracted; // boolean of if life has been taken after player dies

    // variables for player animation
    public Animator animator; // the animator used for the player
    bool moveLeft, moveRight; // booleans of if the player has moved left/right
    public GameObject hitBox; // gameobject that represents player hitbox

    // variables for player audio
    public AudioSource audioSource; // the audiosource for main player sounds
    public AudioClip[] audioClips; // the audio clips for main player sounds

    // initial player properties
    public int initialLives; // the initial number of lives that player has
    public int initialHealth; // the initial health that player has
    public int initialSpeed; // the initial speed of the player
    public float initialShotCooldown; // the initial shot cooldown of the player
    public int maxPowerPoints; // the maximum power points the player can have
    int continues = 3;

    // current player properties
    int health; // the player's health
    int lives; // the number of continues the player has
    int score; // the player's score
    int powerPoints; // the number of power points the player has
    int speed; // the speed of the player
    float shotCooldown; // the fire rate of the player

    // variables for special attack
    public GameObject special; // the special gameobject
    public int specialDuration; // the duration of the special
    public int specialCooldownTime; // cooldown time for special
    float nextSpecial; // the next time player can use special
    bool specialOn; // boolean for if special is used

    // variables for clamping movement
    float clampWidth;
    float clampHeight;
    Vector3 viewPortPosition;

    #region Singleton

    // initialise variables here
    void Awake()
    {
        playerManager = this;

        // set initial properties
        health = initialHealth;
        score = 0;
        lives = initialLives;
        speed = initialSpeed;
        shotCooldown = initialShotCooldown;

        // set invulnerability properties
        invulnerable = false;
        invulnerabilityCooldown = invulnerabilityCooldownTime;
        lifeSubtracted = false;

        // set special attack properties
        special.SetActive(false); 
        specialDuration = 5;
        specialCooldownTime = 3;

        // allow special to be usable from start
        nextSpecial = 0;

        // set up clamp position values
        clampWidth = transform.localScale.x / (Screen.width);
        clampHeight = transform.localScale.y / (Screen.height);
    }

    #endregion

    void Update()
    {
        // handle player invulnerability
        if (invulnerable)
        {
            // disable invulnerability if cooldown is 0
            if (invulnerabilityCooldown <= 0)
            {
                invulnerable = false;
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
                invulnerabilityCooldown = invulnerabilityCooldownTime;
            }
            else
            {
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
            }
        }

        // decrease speed if the player presses the SHIFT button
        if (Input.GetKey(KeyCode.LeftShift))
        {
            hitBox.SetActive(true);
            speed = initialSpeed / 4;
            shotCooldown = initialShotCooldown / 2;
        }
        else
        {
            hitBox.SetActive(false);
            speed = initialSpeed;
            shotCooldown = initialShotCooldown;
        }

        if (!OverlayUIManager.paused)
        {
            // activate the special when X is pressed
            if (!invulnerable && !specialOn && Input.GetKey(KeyCode.X) && Time.time > nextSpecial)
            {
                StartCoroutine(UseSpecial());
            }
            if (invulnerable && invulnerabilityCooldown > 0) { invulnerabilityCooldown--; }
        }

        // manage respawn/continues
        if (health <= 0)
        {
            if (lives > 0)
            {
                // respawn the player
                StartCoroutine(CreateExplosion());
                StartCoroutine(Respawn());
                lifeSubtracted = false;
            }
            else
            {
                OverlayUIManager.overlayUIManager.OpenContinueMenu();
            }
        }

        MovePlayer();
    }

    // getter and setter methods for lives
    public void SetLives(int lives) { this.lives = lives; }
    public int GetLives() { return lives; }

    // getter and setter functions for health
    public int GetHealth() { return health; }
    public void SetHealth(int health) { this.health = health; }

    // getter and setter functions for score
    public int GetScore() { return score; }
    public void SetScore(int score) { this.score = score; }
    public void AddScore(int score) { this.score += score; }

    // getter and setter functions for speed
    public int GetSpeed() { return speed; }
    public void SetSpeed(int speed) { this.speed = speed; }

    // getter and setter functions for shot cooldown
    public float GetShotCooldown() { return shotCooldown; }
    public void SetShotCooldown(float shotCooldown) { this.shotCooldown = shotCooldown; }

    // getter and setter functions for special attack
    public float GetSpecialCooldown() { return specialCooldownTime; }
    public bool GetSpecialOn() { return specialOn; }

    // getter and setter functions for power points
    public void SetPowerPoints(int powerPoints) { this.powerPoints = powerPoints; }
    public int GetPowerPoints() { return powerPoints; }
    public void AddPowerPoints(int upgradePoints) { this.powerPoints += upgradePoints; }
    public int GetMaxPowerPoints() { return maxPowerPoints; }

    // getter and setter function for player continues
    public void SubtractContinues() { if (continues > 0) { continues--; } }
    public int GetContinues() { return continues; }
   
    // getter function for invulnerability
    public bool GetInvulerable() { return invulnerable; }

    // function to move player
    void MovePlayer()
    {
        // player movement
        if (Input.GetKey(KeyCode.UpArrow)) { transform.Translate(Vector2.up * speed * Time.deltaTime); }
        if (Input.GetKey(KeyCode.DownArrow)) { transform.Translate(Vector2.down * speed * Time.deltaTime); }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
            moveLeft = true;
        }
        else { moveLeft = false; }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            moveRight = true;
        }
        else { moveRight = false; }

        // set animation booleans
        animator.SetBool("moveLeft", moveLeft);
        animator.SetBool("moveRight", moveRight);

        viewPortPosition = Camera.main.WorldToViewportPoint(transform.position);
        viewPortPosition.x = Mathf.Clamp(viewPortPosition.x, clampWidth, 1 - clampWidth);
        viewPortPosition.y = Mathf.Clamp(viewPortPosition.y, clampHeight, 1 - clampHeight);
        transform.position = Camera.main.ViewportToWorldPoint(viewPortPosition);
    }

    // function to damage the player
    public void DamagePlayer(int damage)
    {
        // substract health from the player
        health -= damage;
        // flash player to show damage
        StartCoroutine(ShowDamage());
        PlaySound(0);
    }

    // function to play audio
    public void PlaySound(int clip)
    {
        audioSource.clip = audioClips[clip];
        audioSource.Play(); 
    }

    // reset the player when a continue is used 
    public void ResetPlayer()
    {
        lives = initialLives;
        health = initialHealth;
        powerPoints = 0;
        score = 0;

        // instantiate death explosion object and sound
        StartCoroutine(CreateExplosion());
        invulnerable = true;
    }

    IEnumerator UseSpecial()
    {
        specialOn = true;
        special.SetActive(true);
        playerManager.PlaySound(1);
        yield return new WaitForSeconds(specialDuration);
        special.SetActive(false);
        specialOn = false;
        nextSpecial = Time.time + specialCooldownTime;
    }

    // function to respawn player
    IEnumerator Respawn()
    {
        // reset health and power points
        health = initialHealth;
        // subtract life from the player
        if (!lifeSubtracted)
        {
            lives--;
            lifeSubtracted = true;
        }
        // set invulnerability to true
        invulnerable = true;
        yield return null;
    }

    // function to show damage by flashing player red
    IEnumerator ShowDamage()
    {
        GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.5f, 0.5f);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
        yield return new WaitForSeconds(0.2f);
    }

    IEnumerator CreateExplosion()
    {
        // instantiate death explosion object and sound
        GameObject deathExplosionObj = Instantiate(deathExplosion);
        deathExplosionObj.transform.position = transform.position;
        deathExplosion.SetActive(true);
        PlaySound(4);
        yield return null;

        // destroy explosion once it is inactive
        if (!deathExplosionObj.activeSelf) { Destroy(deathExplosion); }
    }
}
