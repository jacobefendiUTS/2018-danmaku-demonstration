﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script for managing player upgrades
public class PlayerUpgradeManager : MonoBehaviour {

    public GameObject[] bulletspawns; // the bulletspawns that are activated 
    int maxPowerPoints; // the maximum powerpoints the player can have
    int powerPoints; // the current powerpoints of the player
    public bool rotation; // boolean for if bullet spawns rotate
    public int initialTurnSpeed; // the starting turnspeed of the bulletspawns
    int turnSpeed; // the current turnspeed of the bullet spawns

    // initialise variables here
    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            bulletspawns[i] = transform.GetChild(i).gameObject;
            bulletspawns[i].SetActive(false);
        }
        turnSpeed = initialTurnSpeed;
    }

    // get variables from other scripts here
    void Start()
    {
        maxPowerPoints = PlayerManager.playerManager.GetMaxPowerPoints();
    }

    void Update()
    {
        // check the current powerpoints of the player
        powerPoints = PlayerManager.playerManager.GetPowerPoints();

        // activate bulletspawns based on current powerpoints
        if (powerPoints >= maxPowerPoints / 4 && !bulletspawns[0].activeSelf)
        {
            PlayerManager.playerManager.PlaySound(2);
            bulletspawns[0].SetActive(true);
        }
        if (powerPoints >= maxPowerPoints / 2 && !bulletspawns[1].activeSelf)
        {
            PlayerManager.playerManager.PlaySound(2);
            bulletspawns[1].SetActive(true);
        }
        if (powerPoints >= 3 * maxPowerPoints / 4 && !bulletspawns[2].activeSelf)
        {
            PlayerManager.playerManager.PlaySound(2);
            bulletspawns[2].SetActive(true);
        }
        if (powerPoints >= maxPowerPoints && !bulletspawns[3].activeSelf)
        {
            bulletspawns[3].SetActive(true);
            PlayerManager.playerManager.PlaySound(2);
        }
        if (powerPoints == 0) { ResetUpgrades(); }

        // manage rotation of bulletspawns
        if (Input.GetKey(KeyCode.LeftShift)) { turnSpeed = initialTurnSpeed * 2; }
        else { turnSpeed = initialTurnSpeed; }
        if (rotation) { transform.Rotate(new Vector3(0, 0, turnSpeed * Time.deltaTime)); }

        // have bullet spawns face upwards while rotating
        for (int i = 0; i < bulletspawns.Length; i++) { bulletspawns[i].transform.eulerAngles = new Vector3(0, 0, 0); }
    }

    // function to deactivate active bulletspawns
    public void ResetUpgrades()
    {
        for (int i = 0; i < bulletspawns.Length; i++) { bulletspawns[i].SetActive(false); }
    }
}
