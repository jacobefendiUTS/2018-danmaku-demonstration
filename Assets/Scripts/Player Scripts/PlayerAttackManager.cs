﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackManager : MonoBehaviour {

    // variables for bullet spawns
    public GameObject primaryBulletSpawn; // the primary bullet spawn
    public GameObject alternateBulletSpawnParent; // the parent object for alternate bullet spawns
    GameObject[] alternateBulletSpawns; // array for alternate bullet spawns

    // variables for firing and cooldown
    GameObject bullet; // the current bullet being fired
    float shotCooldown; // cooldown time for firing
    [SerializeField] float nextFire; // time when player can fire next
    bool canFire; // boolean for if the player can fire bullets (disabled while using special)

    // variables for audio
    public AudioSource audioSource;

    // initialise variables here
    void Awake()
    {
        // get alternate bullet spawns
        alternateBulletSpawns = new GameObject[alternateBulletSpawnParent.transform.childCount];
        for (int i = 0; i < alternateBulletSpawns.Length; i++)
        {
            alternateBulletSpawns[i] = alternateBulletSpawnParent.transform.GetChild(i).gameObject;
        }

        // enable ability to fire
        canFire = true;
    }

    void Update()
    {
        // get current firerate of the player to determine shot cooldown
        shotCooldown = PlayerManager.playerManager.GetShotCooldown();

        // disable firing if special attack is being used
        canFire = !PlayerManager.playerManager.GetSpecialOn();

        if (!OverlayUIManager.paused)
        {
            // shoot regular bullets when Z is pressed
            if (Input.GetKey(KeyCode.Z) && Time.time > nextFire && canFire)
            {
                Fire();
                nextFire = Time.time + shotCooldown;
            }
        }
    }

    // function to fire bullets from active bullet spawns
    void Fire()
    {
       SingleFire(primaryBulletSpawn, "PlayerBullet");

        for (int i = 0; i < alternateBulletSpawns.Length; i++)
        {
            if (alternateBulletSpawns[i].activeSelf)
            {
                SingleFire(alternateBulletSpawns[i], "PlayerUpgradeBullet");
            }
        }
    }

    void SingleFire(GameObject bulletSpawn, string bulletType)
    {
        bullet = ObjectPoolManager.objectPoolManager.GetPooledObject(bulletType);
        bullet.tag = "PlayerBullet";
        bullet.transform.position = bulletSpawn.transform.position;
        bullet.transform.rotation = bulletSpawn.transform.rotation;
        audioSource.Play();
    }

    public void SetCanFire(bool canFire) { this.canFire = canFire; }
}
