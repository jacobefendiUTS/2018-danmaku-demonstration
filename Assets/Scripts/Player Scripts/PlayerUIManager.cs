﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour {

    float initialSpecialCooldown; // the starting cooldown of the player special
    float cooldownTime;
    public Image playerSpecialBar; // the cooldown bar for the player special
    public Text playerScoreText; // the text for the player score

    void Start()
    {
        // set up special cooldown bar
        initialSpecialCooldown = PlayerManager.playerManager.GetSpecialCooldown();
        playerSpecialBar.fillAmount = 1;

        // set up score text
        playerScoreText.text = "0";
    }

    void Update()
    {
        // update special cooldown bar
        initialSpecialCooldown = PlayerManager.playerManager.specialCooldownTime;
        if (PlayerManager.playerManager.GetSpecialOn()) { cooldownTime = 0; }
        else
        {
            cooldownTime += Time.deltaTime;
        }
        playerSpecialBar.fillAmount =  cooldownTime / initialSpecialCooldown;

        // update the score text
        playerScoreText.text = "" + PlayerManager.playerManager.GetScore();
    }
}
