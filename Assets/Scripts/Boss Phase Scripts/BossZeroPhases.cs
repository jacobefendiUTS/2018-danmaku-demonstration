﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossZeroPhases : MonoBehaviour {

    // MAIN BOSS 

    public EnemyMovementManager enemyMovementManager; // the manager script for enemy movement
    public EnemyAttackManager primaryAttackManager; // the manager for the primary attacks
    public EnemyAttackManager alternateAttackManager; // the manager for the secondary attacks
    public GameObject barrier; //  the barrier used by the enemy
    public GameObject deathExplosion; // explosion that is created upon enemy death

    // booleans for if attacks and movements have been set for a phase
    [SerializeField] bool attackSet; // boolean for if attacks have been set
    [SerializeField] bool movementSet; // boolean for if movements have been set 

    // variables for alternating movement
    string primaryMovementType; // the primary movement type used by the main enemy
    string alternateMovementType; // the alternate movement type used by the main enemy
    bool alternateMovement; // boolean for if alternate movement is on
    float nextMovement; 
    float primaryMovementDuration; // time the next movement switch will occur
    float alternateMovementDuration; // the duration of the alternate movement
    int numberOfCompleteMovements; // the number of complete movements made
    bool usingCompleteMovements; // boolean for if alternate movement is determined by cooldown or alternate movements

    // variables for cooldown phase
    bool hasChangedToPowerups; // boolean for if active bullets have changed to powerups
    bool hasResetProperties; // boolean for if enemy attack properties have been reset 

    // SIDE BOSS

    // variables for side boss
    public GameObject sideBoss; // the side boss game object
    public GameObject propellor; // side boss' propellor (for cosmetic purposes)
    EnemyAttackManager sideBossAttackManager; // the side boss' attack manager script
    EnemyMovementManager sideBossMovementManager; // the side boss' movement manager script
    string sideBossPrimaryMovementType; // the primary movement type used by the side boss
    string sideBossAlternateMovementType; // the alternate movement type used by the side boss

    void Awake()
    {
        // turn off the barrier
        barrier.SetActive(false);

        // set the alternate attack variables
        alternateAttackManager.SetAttackType("None");
        primaryAttackManager.SetAttackType("None");

        // set the alternate movement variables
        alternateMovement = false;
    }

    void Start()
    {
        // get side boss' scripts
        sideBossAttackManager = sideBoss.GetComponentInChildren<EnemyAttackManager>();
        sideBossMovementManager = sideBoss.GetComponent<EnemyMovementManager>();
        sideBossAttackManager.SetAttackType("None");
    }


    void Update()
    {
        // find the current phase and therefore determine the attack to use
        if (AttackPhaseManager.attackPhaseManager.IsFirstPhase())
        {
            if (!attackSet && !movementSet) { FirstPhaseAttack(); }
        }

        if (AttackPhaseManager.attackPhaseManager.IsFirstPhaseCooldown())
        {
            CooldownAttack();
        }

        if (AttackPhaseManager.attackPhaseManager.IsSecondPhase()) { SecondPhaseAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsSecondPhaseCooldown()) { CooldownAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsThirdPhase()) { ThirdPhaseAttack(); }
        if (AttackPhaseManager.attackPhaseManager.isDefeated())
        {
            if (!hasChangedToPowerups)
            {
                primaryAttackManager.SetAttackType("None");
                alternateAttackManager.SetAttackType("None");
                ChangeBulletsToPowerUps();
            }
            else
            {
                sideBossAttackManager.gameObject.SetActive(false);
                StartCoroutine(CreateExplosion(transform.position));
            }
        }
        propellor.transform.Rotate(new Vector3(0, 0, 720 * Time.deltaTime));
    }

    void FirstPhaseAttack()
    {
        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode (and properties)
            primaryAttackManager.SetFireMode("Burst");
            primaryAttackManager.SetBurstSize(15);
            primaryAttackManager.SetBurstDelay(0.05f);
            // set bullet type
            primaryAttackManager.SetBulletType("EnemyBullet");
            // set attack type
            primaryAttackManager.SetAttackType("Front");
            // set cooldown time
            primaryAttackManager.SetShotCooldown(3);
            // set swinging/rotation properties
            primaryAttackManager.SetRotation(false);
            primaryAttackManager.SetSwinging(true);
            primaryAttackManager.SetSwingAngle(60);
            primaryAttackManager.SetSwingDuration(0.25f);

            // ALTERNATE ATTACK
            // set fire mode (and properties)
            alternateAttackManager.SetFireMode("Single");
            // set bullet type
            alternateAttackManager.SetBulletType("EnemyBullet");
            // set attack type
            alternateAttackManager.SetAttackType("None");
            // set swinging/rotation properties
            alternateAttackManager.SetRotation(false);
            alternateAttackManager.SetSwinging(false);

            // SIDE BOSS ATTACK
            // set fire mode (and properties)
            sideBossAttackManager.SetFireMode("Circle");
            sideBossAttackManager.SetAngleOffset(10);
            // set bullet type
            sideBossAttackManager.SetBulletType("BigBullet");
            // set attack type
            sideBossAttackManager.SetAttackType("None");
            // set cooldown time
            sideBossAttackManager.SetShotCooldown(5);

            attackSet = true;
        }

        if (!movementSet)
        {
            // set means of switching movement
            usingCompleteMovements = false;

            // PRIMARY MOVEMENT
            // set movement duration
            primaryMovementDuration = 10;
            enemyMovementManager.SetMovementPauseTime(1);
            // set movement properties (if needed)

            // ALTERNATE MOVEMENT
            // set movement duration
            alternateMovementDuration = 5;
            // set movement properties (if needed)

            //SIDE BOSS MOVEMENT
            sideBossMovementManager.SetMovementType("FollowEnemy");
            sideBossMovementManager.SetMaxFollowDistance(3);

            nextMovement = Time.time + primaryMovementDuration;

            movementSet = true;
        }

        ManageMovement(enemyMovementManager, "OrderedWaypoint", "StartingPoint");
        ManageMovement(sideBossMovementManager, "FollowEnemy", "StartingPoint");
    }

    void SecondPhaseAttack()
    {
        barrier.SetActive(false);

        if (hasChangedToPowerups) { hasChangedToPowerups = false; }
        if (hasResetProperties) { hasResetProperties = false; }


        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode
            primaryAttackManager.SetFireMode("Burst");
            primaryAttackManager.SetBurstSize(20);
            primaryAttackManager.SetBurstDelay(0.05f);
            // set bullet type
            primaryAttackManager.SetBulletType("EnemyBullet");
            // set attack type
            primaryAttackManager.SetAttackType("Cone");
            // set cooldown time
            primaryAttackManager.SetShotCooldown(5);
            // set swinging/rotation properties
            primaryAttackManager.SetSwinging(true);

            // ALTERNATE ATTACK
            // set fire mode
            alternateAttackManager.SetFireMode("Circle");
            // set bullet type
            alternateAttackManager.SetBulletType("BigBullet");
            // set attack type
            alternateAttackManager.SetAttackType("None");
            // set cooldown time
            alternateAttackManager.SetShotCooldown(2);
            // set swinging/rotation properties

            // SIDE BOSS
            // set fire mode
            sideBossAttackManager.SetFireMode("Circle");
            // set bullet type
            sideBossAttackManager.SetBulletType("BigBullet");
            // set attack type
            sideBossAttackManager.SetAttackType("None");
            // set cooldown time
            sideBossAttackManager.SetShotCooldown(5);
            // set swinging/rotation properties

            attackSet = true;
        }

        if (!movementSet)
        {
            // set means of switching movements
            usingCompleteMovements = false;

            // PRIMARY MOVEMENT
            // set movement duration
            enemyMovementManager.SetMovementType("StartingPoint");
            primaryMovementDuration = 10;
            // set movement properties (if needed)

            // ALTERNATE MOVEMENT
            // set movement duration
            alternateMovementDuration = 10;
            // set movement properties (if needed)

            // SIDE BOSS MOVEMENT
            sideBossMovementManager.SetMovementType("StartingPoint");
            sideBossMovementManager.SetMaxFollowDistance(3);
            movementSet = true;
        }
    }

    void ThirdPhaseAttack()
    {
        barrier.SetActive(false);
        if (hasChangedToPowerups) { hasChangedToPowerups = false; }
        if (hasResetProperties) { hasResetProperties = false; }

        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode
            primaryAttackManager.SetFireMode("Single");
            // set bullet type
            primaryAttackManager.SetBulletType("EnemyBullet");
            // set attack type
            primaryAttackManager.SetAttackType("Cardinal");
            // set cooldown time
            primaryAttackManager.SetShotCooldown(0.2f);
            // set swinging/rotation properties
            primaryAttackManager.SetRotation(true);   

            // ALTERNATE ATTACK
            // set fire mode
            alternateAttackManager.SetFireMode("Burst");
            alternateAttackManager.SetBurstSize(15);
            alternateAttackManager.SetBurstDelay(0.05f);
            // set bullet type
            alternateAttackManager.SetBulletType("BigBullet");
            // set attack type
            alternateAttackManager.SetAttackType("Omnidirection");
            // set cooldown time
            alternateAttackManager.SetShotCooldown(1);
            // set swinging/rotation properties
            alternateAttackManager.SetSwinging(true);
            alternateAttackManager.enabled = true;

            // SIDE BOSS ATTACKS
            // set fire mode
            sideBossAttackManager.SetFireMode("Circle");
            // set bullet type
            sideBossAttackManager.SetBulletType("BigBullet");
            // set attack type
            sideBossAttackManager.SetAttackType("None");
            // set cooldown time
            alternateAttackManager.SetShotCooldown(3);
            // set swinging/rotation properties
            sideBossAttackManager.SetRotation(false);
            sideBossAttackManager.SetSwinging(false);

            attackSet = true;
        }

        if (!movementSet)
        {
            // PRIMARY MOVEMENT
            // set movement duration
            enemyMovementManager.SetMovementType("StartingPoint");
            // set movement properties (if needed)

            // ALTERNATE MOVEMENT
            // set movement duration
            // set movement properties (if needed)

            // SIDE BOSS MOVEMENT
            // set movement duration
            sideBossMovementManager.SetMovementType("OrderedWaypoint");
            sideBossMovementManager.SetMovementPauseTime(3);
            usingCompleteMovements = true;
            movementSet = true;
        }
    }

    // cooldown attack resets all values and prepare values for next phase
    void CooldownAttack()
    {
        barrier.SetActive(true);
        sideBossMovementManager.SetMovementType("FollowPlayer");
        sideBossMovementManager.SetMaxFollowDistance(0);

        if (!hasChangedToPowerups) { ChangeBulletsToPowerUps(); }

        if (!hasResetProperties)
        {
            // reset primary attack manager properties
            attackSet = false;
            primaryAttackManager.resetAttackProperties();
            primaryAttackManager.ResetRotationProperties();
            primaryAttackManager.ResetSwingProperties();
            primaryAttackManager.SetAttackType("None");

            // reset alternate attack manager properties
            alternateAttackManager.resetAttackProperties();
            alternateAttackManager.ResetRotationProperties();
            alternateAttackManager.ResetSwingProperties();
            alternateAttackManager.SetAttackType("None");

            //  reset side boss properties
            sideBossAttackManager.resetAttackProperties();
            sideBossAttackManager.ResetRotationProperties();
            sideBossAttackManager.ResetSwingProperties();
            sideBossAttackManager.SetAttackType("None");

            // reset the alternate movement properties
            movementSet = false;
            alternateMovement = false;
            enemyMovementManager.SetMovementType("StartingPoint");

            hasResetProperties = true;
        }
    }

    void ManageMovement(EnemyMovementManager movementManager, string primaryMovementType, string alternateMovementType)
    {
        // activate alternate movement if complete movement has been made or cooldown runs out
        if (!alternateMovement)
        {
            if (usingCompleteMovements)
            {
                numberOfCompleteMovements = movementManager.GetCompleteMovements();
                if (numberOfCompleteMovements > 1)
                {
                    alternateMovement = true;
                    StartCoroutine(UseAlternateMovement(movementManager, primaryMovementType, alternateMovementType));
                }
            }
            else if (Time.time > nextMovement)
            {
                alternateMovement = true;
                StartCoroutine(UseAlternateMovement(movementManager, primaryMovementType, alternateMovementType));
            }
            else
            {
                movementManager.SetMovementType(primaryMovementType);
            }
        }
    }

    IEnumerator UseAlternateMovement(EnemyMovementManager movementManager, string primaryMovementType, string alternateMovementType)
    {
        Debug.Log("Using Alternate Movement");
        movementManager.SetMovementType(alternateMovementType);
        yield return new WaitForSeconds(alternateMovementDuration);
        movementManager.SetMovementType(primaryMovementType);
        alternateMovement = false;
        nextMovement = Time.time + primaryMovementDuration;
        yield return null;
    }

    void ChangeBulletsToPowerUps()
    {
        string primaryBulletType = primaryAttackManager.GetBulletType();
        string alternateBulletType = alternateAttackManager.GetBulletType();
        ObjectPoolManager.objectPoolManager.ChangeToObject(primaryBulletType, "ScorePowerUp");
        ObjectPoolManager.objectPoolManager.ChangeToObject(alternateBulletType, "UpgradePowerUp");
        hasChangedToPowerups = true;
    }

    IEnumerator CreateExplosion(Vector3 position)
    {
        // instantiate death explosion object and sound
        GameObject deathExplosionObj = Instantiate(deathExplosion);
        deathExplosionObj.transform.position = position;
        deathExplosion.SetActive(true);
        yield return null;
        if (!deathExplosionObj.activeSelf) { Destroy(deathExplosionObj); }
        sideBoss.SetActive(false);
        gameObject.SetActive(false);
    }
}
