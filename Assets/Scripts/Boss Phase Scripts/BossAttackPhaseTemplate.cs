﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackPhaseTemplate : MonoBehaviour {


    public EnemyMovementManager enemyMovementManager; // the manager script for enemy movement
    public EnemyAttackManager primaryAttackManager; // the manager for the primary attacks
    public EnemyAttackManager alternateAttackManager; // the manager for the secondary attacks
    public GameObject barrier; //  the barrier used by the enemy

    [SerializeField] bool attackSet; // boolean for if attacks have been set
    [SerializeField] bool movementSet; // boolean for if movements have been set 

    // variables for alternate attacks
    public int initialAlternateAttackCooldownTime; // the starting alternate attack cooldown time
    public int initialAlternateAttackDuration; // the starting alternate attack duration
    bool alternateAttack; // boolean for if alternate attack is on
    int alternateAttackCooldown; // current cooldown of alternate attack
    int alternateAttackCooldownTime; // cooldown time of alternate attack
    int alternateAttackDuration; // the duration of the alternate attack
    int alternateAttackUseTime; // the current use time of the alternate attack

    // variables for alternate movement
    public int initialAlternateMovementCooldownTime; // the starting alternate attack cooldown time
    public int initialAlternateMovementDuration; // the starting duration of the alternate movement
    int alternateMovementCooldownTime; // the cooldown time of the alternate movement
    int alternateMovementDuration; // the duration of the alternate movement
    bool alternateMovement; // boolean for if alternate movement is on
    int alternateMovementCooldown; // the cooldown of the alternate movement
    int alternateMovementUseTime; // the use time of the alternate movement
    bool usingCompleteMovements; // boolean for if alternate movement is determined by cooldown or alternate movements

    // variables for cooldown phase
    bool hasChangedToPowerups; // boolean for if active bullets have changed to powerups
    bool hasResetProperties; // boolean for if enemy attack properties have been reset 

    void Awake()
    {
        barrier.SetActive(false);

        // set the alternate attack variables
        alternateAttackCooldownTime = initialAlternateAttackCooldownTime;
        alternateAttackDuration = initialAlternateAttackDuration;
        alternateAttackUseTime = alternateAttackDuration;
        alternateAttackCooldown = alternateAttackCooldownTime;
        alternateAttack = false;
        alternateAttackManager.SetAttackType("None");
        primaryAttackManager.SetAttackType("None");

        // set the alternate movement times
        alternateMovementCooldownTime = initialAlternateMovementCooldownTime;
        alternateMovementDuration = initialAlternateMovementDuration;
        alternateMovementCooldown = alternateMovementCooldownTime;
        alternateMovementUseTime = alternateMovementDuration;
    }


    void Update()
    {
        // find the current phase and therefore determine the attack to use
        if (AttackPhaseManager.attackPhaseManager.IsFirstPhase()) { firstPhaseAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsFirstPhaseCooldown()) { cooldownAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsSecondPhase()) { secondPhaseAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsSecondPhaseCooldown()) { cooldownAttack(); }
        if (AttackPhaseManager.attackPhaseManager.IsThirdPhase()) { thirdPhaseAttack(); }
        if (AttackPhaseManager.attackPhaseManager.isDefeated())
        {
            if (!hasChangedToPowerups)
            {
                primaryAttackManager.SetAttackType("None");
                alternateAttackManager.SetAttackType("None");
                ChangeBulletsToPowerUps();
            }
            else { gameObject.SetActive(false); }
        }
    }

    void firstPhaseAttack()
    {
        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            // ALTERNATE ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            attackSet = true;
        }

        if (!movementSet)
        {
            // PRIMARY MOVEMENT
            // set movement type
            // set means of switching
            usingCompleteMovements = true;

            // ALTERNATE MOVEMENT
            // set movement type
            // set movement duration
            alternateMovementDuration = 200;
            alternateMovementUseTime = alternateMovementDuration;
            // set movement cooldowns (if applicable)

            movementSet = true;
        }

        // manage cooldowns if game is not paused()
        if (!OverlayUIManager.paused)
        {
            ManageAlternateAttack();
            ManageMovement("StartingPoint", "StartingPoint");
        }
    }

    void secondPhaseAttack()
    {
        barrier.SetActive(false);
        if (hasChangedToPowerups) { hasChangedToPowerups = false; }
        if (hasResetProperties) { hasResetProperties = false; }

        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            // ALTERNATE ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            attackSet = true;
        }

        if (!movementSet)
        {
            // PRIMARY MOVEMENT
            // set movement type
            // set means of switching

            // ALTERNATE MOVEMENT
            // set movement type
            // set movement duration
            // set movement cooldowns (if applicable)

            movementSet = true;
        }

        // manage cooldowns if game is not paused()
        if (!OverlayUIManager.paused)
        {
            ManageAlternateAttack();
            ManageMovement("StartingPoint", "StartingPoint");
        }
    }

    void thirdPhaseAttack()
    {
        barrier.SetActive(false);
        if (hasChangedToPowerups) { hasChangedToPowerups = false; }
        if (hasResetProperties) { hasResetProperties = false; }

        if (!attackSet)
        {
            // PRIMARY ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            // ALTERNATE ATTACK
            // set fire mode
            // set bullet type
            // set attack type
            // set cooldown time
            // set swinging/rotation properties

            attackSet = true;
        }

        if (!movementSet)
        {
            // PRIMARY MOVEMENT
            // set movement type
            // set means of switching

            // ALTERNATE MOVEMENT
            // set movement type
            // set movement duration
            // set movement cooldowns (if applicable)
            movementSet = true;
        }
    }

    // cooldown attack resets all values and prepare values for next phase
    void cooldownAttack()
    {
        barrier.SetActive(true);
        if (!hasChangedToPowerups) { ChangeBulletsToPowerUps(); }

        if (!hasResetProperties)
        {
            // reset primary attack manager properties
            attackSet = false;
            primaryAttackManager.resetAttackProperties();
            primaryAttackManager.ResetRotationProperties();
            primaryAttackManager.ResetSwingProperties();
            primaryAttackManager.SetAttackType("None");

            // reset alternate attack manager properties
            alternateAttackManager.resetAttackProperties();
            alternateAttackManager.ResetRotationProperties();
            alternateAttackManager.ResetSwingProperties();
            alternateAttackManager.SetAttackType("None");

            // reset the alternate movement properties
            movementSet = false;
            alternateMovement = false;
            enemyMovementManager.SetMovementType("StartingPoint");

            // reset alternate attack properties
            alternateAttack = false;
            alternateAttackCooldown = alternateAttackCooldownTime;
            alternateAttackUseTime = alternateAttackDuration;
            alternateMovementCooldown = alternateMovementCooldownTime;
            alternateMovementUseTime = alternateMovementDuration;

            alternateAttackManager.SetTurnSpeed(alternateAttackManager.GetInitialTurnSpeed());
            alternateAttackCooldownTime = initialAlternateAttackCooldownTime;
            alternateAttackDuration = initialAlternateAttackDuration;
            alternateMovementCooldownTime = initialAlternateMovementCooldownTime;
            alternateMovementDuration = initialAlternateMovementDuration;

            hasResetProperties = true;
        }
    }

    // function to manage alternate attack activation
    void ManageAlternateAttack()
    {
        if (alternateAttack) { alternateAttackUseTime--; }
        if (alternateAttackCooldown > 0) { alternateAttackCooldown--; }

        // activate the alternate attack when the cooldown is 0
        if (alternateAttackCooldown <= 0)
        {
            alternateAttack = true;
            alternateAttackManager.enabled = true;
        }

        // if alternate attack use time runs out, turn off the alternate attack
        if (alternateAttack && alternateAttackUseTime <= 0)
        {
            // turn off the attack
            alternateAttack = false;
            alternateAttackManager.enabled = false;

            // reset alternate attack properties and cooldowns
            alternateAttackCooldown = alternateAttackCooldownTime;
            alternateAttackUseTime = alternateAttackDuration;
        }
    }

    // function to manage enemy movement
    void ManageMovement(string primaryMovementType, string alternateMovementType)
    {
        // check for when to set alternate movement
        if (usingCompleteMovements)
        {
            // switch movement patterns if the enemy has reached all waypoints
            if (enemyMovementManager.GetCompleteMovements() >= 1)
            {
                alternateMovement = true;
                enemyMovementManager.ResetCompleteMovements();
            }
        }
        else
        {
            // switch movement patterns if cooldown has reached zero
            if (alternateMovementCooldown <= 0)
            {
                alternateMovement = true;
                enemyMovementManager.SetMovementType(alternateMovementType);
            }
        }

        // if alternate movement use time runs out, turn off the alternate movement
        if (alternateMovement && alternateMovementUseTime <= 0)
        {
            alternateMovement = false;
            alternateMovementCooldown = alternateMovementCooldownTime;
            alternateMovementUseTime = initialAlternateMovementDuration;
            enemyMovementManager.ResetTargetIndex();
        }

        if (alternateMovement)
        {
            enemyMovementManager.SetMovementType(alternateMovementType);
            alternateMovementUseTime--;
        }
        else
        {
            enemyMovementManager.SetMovementType(primaryMovementType);
            if (!usingCompleteMovements) { alternateMovementCooldown--; }
        }
    }

    void ChangeBulletsToPowerUps()
    {
        string primaryBulletType = primaryAttackManager.GetBulletType();
        string alternateBulletType = alternateAttackManager.GetBulletType();
        ObjectPoolManager.objectPoolManager.ChangeToObject(primaryBulletType, "ScorePowerUp");
        ObjectPoolManager.objectPoolManager.ChangeToObject(alternateBulletType, "UpgradePowerUp");
        hasChangedToPowerups = true;
    }
}
