﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackManager : MonoBehaviour {

    // variables for setting up attacks
    public EnemyManager enemyManager;
    public GameObject[] bulletSpawns;
    public string bulletType;
    public string attackType;
    public string fireMode;

    // variables for firing bullets
    GameObject bullet; // the current bullet being fired
    [SerializeField] float shotCooldown; // the current point in cooldown
    float nextFire; // time when enemy is able to fire a shot
    public int soundType; // the sound played by the bullet

    // variables for burstFire
    public bool burstFireOn = false;
    public int burstSize;
    public float burstDelay;

    // variables for circular fire
    public bool circleFireOn = false;
    public int angleOffset = 10;
    public int maxDegrees = 360;

    // variables for rotation movement
    bool rotation; // boolean for if gameobject is rotating
    public int initialTurnSpeed; // the starting turnspeed
    int turnSpeed; // the turning speed of the gameobject

    // variables for swinging movement
    bool swinging; // boolean for if gameobject is swinging
    public float initialSwingPeriod; // the starting swing duration
    public float initialSwingAngle; // the starting swing angle
    float swingTime; // the current time for the swing
    float swingPeriod; // the duration of the swing
    float swingAngle; // the angle of rotation that the gameobject swings to

    // initialise variables here
    void Awake()
    {
        // get bulletspawns
        bulletSpawns = new GameObject[transform.childCount];
        GetBulletSpawns();

        // set attack and fire type
        SetFireMode("Single");
        SetAttackType("None");

        // set swinging/rotation properties
        swinging = false;
        rotation = false;
        swingPeriod = initialSwingPeriod;
        swingAngle = initialSwingAngle;
        turnSpeed = initialTurnSpeed;
        nextFire = 1;
    }

    void Update()
    {
        // rotate or swing object if they have been toggled on
        if (rotation) { transform.Rotate(new Vector3(0, 0, turnSpeed * Time.deltaTime)); }
        else if (swinging)
        {
            swingTime = swingTime + Time.deltaTime; // determine swing time of the object
            float phase = Mathf.Sin(swingTime / swingPeriod); // determine phase of swing
            // rotate the object in sine wave movement
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, phase * swingAngle));
        }
        // straighten the object if it is not rotating or swinging
        else { transform.rotation = Quaternion.identity; }

        if (Time.time > nextFire)
        {
            nextFire = Time.time + shotCooldown;
            Fire();
        }
    }

    // function to fire bullets
    void Fire()
    {
        for (int i = 0; i < bulletSpawns.Length; i++)
        {
            if (bulletSpawns[i].activeSelf)
            {
                if (burstFireOn) { StartCoroutine(BurstFire(bulletSpawns[i], bulletType)); }
                else { SingleFire(bulletSpawns[i], bulletType); }
            }
        }
        if (attackType == "None" && circleFireOn) { StartCoroutine(CircleFire()); }
    }

    // function for single fire mode
    void SingleFire(GameObject bulletSpawn, string bulletType)
    {
        // get a bullet from the pool
        bullet = ObjectPoolManager.objectPoolManager.GetPooledObject(bulletType);
        // tag the bullet as an enemy bullet
        bullet.tag = "EnemyBullet";
        // set the position and rotation of bullet to that of bulletspawn
        bullet.transform.position = bulletSpawn.transform.position;
        bullet.transform.rotation = bulletSpawn.transform.rotation;
        enemyManager.playBulletSound(soundType);
    }

    // coroutine for burst fire mode
    IEnumerator BurstFire(GameObject bulletSpawn, string bulletType)
    {
        // fire an amount of bullets specified by the burstSize
        for (int i = 0; i < burstSize; i++)
        {
            // fire the bullet
            SingleFire(bulletSpawn, bulletType);
            // delay the next shot
            yield return new WaitForSeconds(burstDelay);
        }
    }

    // coroutine for circle fire mode
    IEnumerator CircleFire()
    {
        for (int i = 0; i < maxDegrees; i += angleOffset)
        {
            // get the next angle to fire bullet from
            Quaternion angle = Quaternion.Euler(0, 0, i);

            // fire the bullet
            SingleFire(gameObject, bulletType);
            bullet.transform.rotation = angle;
        }
        yield return null;
    }

    // function that sets the attack type based off the value given
    public void SetAttackType(string attackType)
    {
        this.attackType = attackType;
        switch (attackType)
        {
            case "None": // non-attack, deactivate all bullet spawns
                SetBulletSpawns(false);
                break;
            case "Front": // front shot, activate only the front bullet spawn
                bulletSpawns[0].gameObject.SetActive(true);
                break;
            case "Cardinal": // four direction shot, activate the front, back, left and right bullet spawns
                for (int i = 0; i < 8; i += 2) { bulletSpawns[i].gameObject.SetActive(true); }
                break;
            case "Ordinal": // four direction shot, activate the front-left, front-right, back-left and back-right spawns
                for (int i = 1; i < 8; i += 2) { bulletSpawns[i].gameObject.SetActive(true); }
                break; ;
            case "Omnidirection": // omnidirectional shot, activate all bullet spawns
                SetBulletSpawns(true);
                break;
            case "Cone": // front cone shot, activate the front, front-left and front-right bullet spawns
                bulletSpawns[0].gameObject.SetActive(true);
                bulletSpawns[1].gameObject.SetActive(true);
                bulletSpawns[7].gameObject.SetActive(true);
                break;
            default: // invalid value entered, deactivate all bullet spawns
                SetBulletSpawns(false);
                break;
        }
    }

    // function that sets firing mode based off value given
    public void SetFireMode(string fireMode)
    {
        this.fireMode = fireMode;
        switch(fireMode)
        {
            case "Single": // single shot from each bullet spawn
                burstFireOn = false;
                circleFireOn = false;
                break;
            case "Burst": // burst shot from each bullet spawn
                burstFireOn = true;
                circleFireOn = false;
                break;
            case "Circle": // circle fire
                burstFireOn = false;
                circleFireOn = true;
                break;
            default: // default to single shot firing
                burstFireOn = false;
                circleFireOn = false;
                break;
        }
    }

    // function to reset attack properties
    public void resetAttackProperties()
    {
        SetAttackType("None");
        SetFireMode("Single");
        SetBulletType("EnemyBullet");
    } 

    // function that retrieves all bullet spawns from parent object
    void GetBulletSpawns()
    {
        for (int i = 0; i < bulletSpawns.Length; i++) { bulletSpawns[i] = transform.GetChild(i).gameObject; }
    }

    // function that activates/deactivates all bullet spawns
    void SetBulletSpawns(bool enabled)
    {
        for (int i = 0; i < bulletSpawns.Length; i++) { bulletSpawns[i].gameObject.SetActive(enabled); }
    }

    // function to set cooldown time
    public void SetShotCooldown(float shotCooldown) { this.shotCooldown = shotCooldown; }
    
    // function to set burst fire properties
    public void SetBurstDelay(float burstDelay) { this.burstDelay = burstDelay; }
    public void SetBurstSize(int burstSize) { this.burstSize = burstSize; }

    //  function to set circular fire properties
    public void SetAngleOffset(int angleOffset) { this.angleOffset = angleOffset; }
    public void setMaxDegrees(int maxDegrees) { this.maxDegrees = maxDegrees; }

    // functions to get initial properties
    public int GetInitialTurnSpeed() { return initialTurnSpeed; }
    public float GetInitialSwingDuration() { return initialSwingPeriod; }
    public float GetinitialSwingAngle() { return initialSwingAngle; }

    // functions to get/set bullet types
    public void SetBulletType(string bulletType) { this.bulletType = bulletType; }
    public string GetBulletType() { return bulletType; }

    // functions to set rotation properties
    public void SetRotation(bool rotation) { this.rotation = rotation; }
    public void SetTurnSpeed(int turnSpeed) { this.turnSpeed = turnSpeed; }
    public void ResetRotationProperties()
    {
        turnSpeed = initialTurnSpeed;
        SetRotation(false);
    }

    // functions to set swing properties
    public void SetSwinging(bool swinging) { this.swinging = swinging; }
    public void SetSwingDuration(float swingDuration) { this.swingPeriod = swingDuration; }
    public void SetSwingAngle(float swingAngle) { this.swingAngle = swingAngle; }
    public void ResetSwingProperties()
    {
        swingPeriod = initialSwingPeriod;
        swingAngle = initialSwingAngle;
        SetSwinging(false);
    }
}
