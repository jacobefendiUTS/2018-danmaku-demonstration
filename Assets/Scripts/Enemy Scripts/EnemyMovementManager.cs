﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementManager : MonoBehaviour
{
    Animator animator; // animator for enemy character
    [SerializeField] string movementType; // the type of movement used
    [SerializeField] bool moving; // boolean for if object is moving (mainly for animation)
    Vector3 startingPoint; // the position where the object first spawns
    [SerializeField] Vector3 target; // the position where the enemy is moving towards
    Vector3 direction; // the next direction that the enemy moves in
    float distance; // the distance from the object to the target waypoint
    float smoothTime; // the time enemy will take to reach target (using smoothdamp)

    // variables for movement pause/cooldown (random movement and waypoint movement)
    public float movementPauseTime; // the time the movement is paused for
    float nextMovement; // the time until the next movement

    // variables for side-to-side and random direction movement
    bool directionSpecified; // boolean for if a direction to move in has been specified
    float movementMultiplier;

    // variables for waypoint based movement
    public GameObject waypointsParent; // the parent object that contains waypoints
    [SerializeField] List<Transform> waypoints; // list to contain waypoints used by enemy
    [SerializeField] int targetIndex; // the index of the target waypoint
    int completeMovements; // the number of times the enemy was reached all waypoints

    // variables for following movement
    GameObject enemy; // enemy target
    GameObject player; // player target
    float maxFollowDistance; // the maximum distance the enemy moves before it stops following target

    // variables for moving to starting point
    bool atStartingPoint;

    // initialise variables here
    private void Awake()
    {
        // get enemy animator
        animator = GetComponent<Animator>();
        // get the waypoints
        if (waypointsParent.transform.childCount > 0) { GetWaypoints(); }
        // set target waypoints index to first waypoints
        targetIndex = 0;
        // set movement cooldown time
        nextMovement = movementPauseTime;
        // set default smooth time
        smoothTime = 0.125f;
        // set default movement multiplier
        movementMultiplier = 5;
    }

    void Start()
    {
        // translate enemy to starting point
        startingPoint = transform.position;
        transform.position = startingPoint;

        // find enemy and player targets
        if (enemy == null) { enemy = GameObject.FindGameObjectWithTag("Enemy"); }
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // use update to update the character animations
    void Update() { if (animator != null) { animator.SetBool("moving", moving); } }

    // use fixedupdate to update movement (since smooth damp is involved)
    void FixedUpdate() { Move(); }

    // function to toggle movement type
    void Move()
    {
        switch (movementType)
        {
            case "None": // case 0: no movement
                moving = false;
                break;
            case "Random": // case 2: random movement
                RandomMovement(); 
                break;
            case "OrderedWaypoint": // case 3: ordered waypoint movement
                // set target to current target waypoint
                target = waypoints[targetIndex].position;
                WayPointMovement(false); 
                break;
            case "RandomWaypoint": // case 4: random waypoint movement
                // set target to current target waypoint
                target = waypoints[targetIndex].position;
                WayPointMovement(true); 
                break;
            case "StartingPoint": // return to starting point
                MoveToStartingPoint(); 
                break;
            case "FollowEnemy": // follow designated enemy
                // set target to enemy
                target = enemy.transform.position;
                FollowTargetMovement(); 
                break;
            case "FollowPlayer": // follow player
                // set target to player
                target = player.transform.position;
                FollowTargetMovement(); 
                break;
        }
    }

    // function for random direction movement
    void RandomMovement()
    {
        if (Time.time > nextMovement)
        {
            // move if the direction has been specified
            if (directionSpecified)
            {
                // calculate distance between target and enemy
                distance = Vector3.Distance(transform.position, target);

                // if the object is still not within a certain distance of the point continue moving
                if (distance >= 0.3f)
                {
                    // if the target is far enough, turn on moving boolean
                    moving = true; 

                    // move the object in that direction
                    MoveToTarget(target, false);
                }
                else
                {
                    // pause movement
                    nextMovement = Time.time + movementPauseTime;
                    // find a new direction once enemy can move again
                    directionSpecified = false;
                }
            }
            else
            {
                // get direction based on random point inside unit circle
                direction = Random.insideUnitCircle;
                directionSpecified = true;
                // assign the target to be the new direction
                target = transform.up + direction * movementMultiplier;
            }
        }
        else { moving = false; }
    }

    // function for moving back to starting point
    void MoveToStartingPoint()
    {
        // calculate the distance between the object and the starting point
        distance = Vector3.Distance(transform.position, startingPoint);
        if (distance >= 0.1f)
        {
            // if the object is still not within a certain distance of the point continue moving
            // calculate the velocity/direction based on the starting point
            // move the object in that direction
            moving = true;
            MoveToTarget(startingPoint, false);
        }
        else
        {
            // make the object remain still at the starting point
            moving = false;
            atStartingPoint = true;
        }
    }

    // function for waypoints based movement
    void WayPointMovement(bool isRandom)
    {
        if (Time.time > nextMovement)
        {
            // calculate the distance between the object and the target point
            distance = Vector3.Distance(transform.position, target);
            // if the object is still not within a certain distance of the target waypoint continue moving
            if (distance > 0.5f) { MoveToTarget(target, true); }
            else
            {
                // if the movement is set to random, specify the next index randomly
                if (isRandom) { targetIndex = Random.Range(0, waypoints.Count); }
                else
                {
                    // if the movement is set to ordered, specify the target index as the next one in the list
                    if (targetIndex < waypoints.Count - 1)
                    {
                        targetIndex++;
                    }
                    else
                    {
                        // if the enemy has reached the final waypoint, set target to first waypoint
                        targetIndex = 0;
                        completeMovements++;
                    }
                    nextMovement = Time.time + movementPauseTime;
                }
            }
            // update the target
            target = waypoints[targetIndex].position;
        }
        else { moving = false; }
    }

    // function for target following movement
    void FollowTargetMovement()
    {
        if (target == player.transform.position)
        {
            distance = Vector3.Distance(transform.position, target);
            if (distance > maxFollowDistance) { MoveToTarget(target, true); }
        }
        else
        {
            // follow the target while maintaining being maxDistance units away from the target
            Vector3 followTarget = (transform.position - target).normalized * maxFollowDistance + target;
            transform.position = Vector3.SmoothDamp(transform.position, followTarget, ref direction, smoothTime);
        }
    }


    // function to move to specified point
    void MoveToTarget(Vector3 targetPosition, bool normalized)
    {
        // normalise the vector if requested
        if (normalized) { direction = (transform.position - targetPosition).normalized; }
        else { direction = (transform.position - targetPosition); }

        // move the enemy towards the target using smoothdamp
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref direction, smoothTime);
    }

    // function to get waypoints used by player
    void GetWaypoints()
    {
        // make the list of waypoints
        waypoints = new List<Transform>();

        // add the waypoints to the list
        for (int i = 0; i < waypointsParent.transform.childCount; i++)
        {
            Transform waypoint = waypointsParent.transform.GetChild(i);
            waypoint.gameObject.name = "Waypoint [" + i + "]";
            waypoints.Add(waypoint);
        }

        // set the first waypoint as the target
        target = waypoints[0].position;
    }

    // set the movement type of the enemy
    public void SetMovementType(string movementType)
    {
        this.movementType = movementType;
        moving = true;
        atStartingPoint = false;
    }

    // setter methods for universally used variables
    public void SetMovementPauseTime(float movementPauseTime) { this.movementPauseTime = movementPauseTime; }
    public void SetSmoothTime(float smoothTime) { this.smoothTime = smoothTime; }

    // setter methods for random movement variables
    public void SetMovementMultiplier(float movementMultiplier) { this.movementMultiplier = movementMultiplier; }

    // setter/getter methods for waypoint movement variables
    public int GetCompleteMovements() { return completeMovements; }
    public void ResetCompleteMovements() { completeMovements = 0; }
    public void ResetTargetIndex() { targetIndex = 0; }

    // getter method for if enemy is at starting point
    public bool IsAtStartingPoint() { return atStartingPoint; }

    // setter method for maximum follow distance distance
    public void SetMaxFollowDistance(float maxFollowDistance) { this.maxFollowDistance = maxFollowDistance; }
}
