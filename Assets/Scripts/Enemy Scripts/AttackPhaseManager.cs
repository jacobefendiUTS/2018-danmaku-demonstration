﻿using UnityEngine;

public class AttackPhaseManager : MonoBehaviour {

    public static AttackPhaseManager attackPhaseManager;
    public EnemyManager enemyManager; // enemymanager script
    int initialHealth; // the initial health of the enemy
    int health; // the current health of the enemy

    // variables for cooldown phases
    public float phaseCooldownDuration; // the starting cooldown duration
    float phaseCooldown; // current cooldown time for enemy

    // booleans for battle phases
    [SerializeField] bool firstPhase; // boolean for if first phase is done
    [SerializeField] bool firstPhaseCooldown; // boolean for if first phase cooldown is done
    [SerializeField] bool secondPhase; // boolean for if second phase is done
    [SerializeField] bool secondPhaseCooldown; // boolean for if second phase cooldown is done
    [SerializeField] bool thirdPhase; // boolean for if third phase is done 
    [SerializeField] bool defeated; // boolean for if the enemy has been defeated

    // initialise variables here
    void Awake()
    {
        attackPhaseManager = this;
        // set the cooldown times
        phaseCooldown = phaseCooldownDuration;
        // set the first phase
        firstPhase = true;
    }

    // initialise variables from other scripts here
    void Start()
    {
        // set the health of the enemy
        initialHealth = enemyManager.initialHealth;
        health = initialHealth;
    }

    void Update()
    {
        health = enemyManager.GetHealth(); // get the current health of the enemy

        // first phase attack
        if (firstPhase)
        {
            // continue attack while health is within the given range
            if (health <= initialHealth && health > 3 * initialHealth / 4) { }
            else // otherwise deactivate the first phase and go into the cooldown phases
            {
                firstPhase = false;
                firstPhaseCooldown = true;
            }
        }

        // first phase cooldown
        if (firstPhaseCooldown)
        {
            // stay on cooldown while the timer is still active
            if (!OverlayUIManager.paused && phaseCooldown >= 0) { phaseCooldown -= Time.deltaTime; }
            else // otherwise go into the second phase
            {
                firstPhaseCooldown = false;
                secondPhase = true;
                phaseCooldown = phaseCooldownDuration; // reset the cooldown timer
            }
        }

        // second phase attack
        if (secondPhase)
        {
            // continue attack while health is in the given range
            if (health <= 3 * initialHealth / 4 && health > initialHealth / 2) { }
            else // otherwise go into the next phase
            {
                secondPhase = false;
                secondPhaseCooldown = true;
            }
        }

        // first phase cooldown
        if (secondPhaseCooldown)
        {
            // stay on cooldown while the timer is still active
            if (!OverlayUIManager.paused && phaseCooldown >= 0) { phaseCooldown -= Time.deltaTime; }
            else // otherwise go into the second phase
            {
                secondPhaseCooldown = false;
                thirdPhase = true;
                phaseCooldown = phaseCooldownDuration; // reset the cooldown timer
            }
        }

        // third phase attack
        if (thirdPhase)
        {
            // continue attack while health is in the given range
            if (health <= initialHealth / 2 && health > 0) { }
            else
            {
                thirdPhase = false;
                defeated = true;
            } 
        }
    }

    // functions to return current phase
    public bool IsFirstPhase() { return firstPhase; }
    public bool IsFirstPhaseCooldown() { return firstPhaseCooldown; }
    public bool IsSecondPhase() { return secondPhase; }
    public bool IsSecondPhaseCooldown() { return secondPhaseCooldown; }
    public bool IsThirdPhase() { return thirdPhase; }
    public bool isDefeated() { return defeated; }
}
