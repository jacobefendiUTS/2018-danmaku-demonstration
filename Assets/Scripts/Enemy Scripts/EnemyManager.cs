﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script to contain properties of the enemy
public class EnemyManager : MonoBehaviour {

    public string name; // name of the enemy
    public int initialHealth; // the starting health of the enemy
    public int initialSpeed; // the starting speed of the enemy
    int health; // the current enemy health
    int speed; // the current enemy speed

    // variables for audio
    public AudioSource enemyAudioSource; // the audiosource for general sounds
    public AudioSource bulletAudioSource; // the audiosource for bullet sounds
    public AudioClip[] enemyAudioClips; // the audioclips for enemy
    public AudioClip[] bulletAudioClips; // the audioclips for enemy bullets

    // initialise enemy properties here
    void Awake()
    {
        health = initialHealth;
        speed = initialSpeed;
    }

    // getter and setter methods for enemy properties
    public string GetName() { return name; }
    public void SetName(string name) { this.name = name; }
    public int GetHealth() { return health; }
    public int GetSpeed() { return speed; }
    public void SetSpeed(int speed) { this.speed = speed; }

    // function to subtract health from enemy
    public void damageEnemy(int damage)
    {
        if (health > 0)
        {
            health -= damage;
            enemyAudioSource.clip = enemyAudioClips[0];
            enemyAudioSource.Play();
        }
    }

    // function to play bullet firing sounds
    public void playBulletSound(int clip)
    {
        bulletAudioSource.clip = bulletAudioClips[clip];
        bulletAudioSource.Play();
    }

}
