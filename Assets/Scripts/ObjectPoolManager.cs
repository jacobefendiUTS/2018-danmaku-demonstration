using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour {

    // class for pool of objects
    [System.Serializable]
    public class PooledObject
    {
        public string tag;
        public GameObject obj;
        public int amountToPool;
        public bool shouldExpand;
    }
    
    // lists containing objects that are to be pooled
    public List<PooledObject> pooledObjects;

    // dictionary containing all object pools
    public Dictionary<string, List<GameObject>> objectPoolDictionary;

    #region Singleton

    public static ObjectPoolManager objectPoolManager;

    // initialise variables here
    void Awake()
    {
        objectPoolManager = this;

        // initialise object pool dictionary
        objectPoolDictionary = new Dictionary<string, List<GameObject>>();

        // go through each object pool in the object pools list
        foreach (PooledObject pooledObject in pooledObjects)
        {
            // create a new list to contain objects from pool
            List<GameObject> objectPool = new List<GameObject>();

            // create all objects to add to list
            for (int i = 0; i < pooledObject.amountToPool; i++)
            {
                GameObject obj = Instantiate(pooledObject.obj);
                obj.SetActive(false);
                objectPool.Add(obj);
            }

            // add objects to dictionary with tag
            objectPoolDictionary.Add(pooledObject.tag, objectPool);
        }

    }

    #endregion

    // function to get object from specified pool
    public GameObject GetPooledObject(string tag)
    {
        //find list in dictionary with matching tag
        if (objectPoolDictionary.ContainsKey(tag))
        {
            // find an inactive object to return
            foreach (GameObject obj in objectPoolDictionary[tag])
            {
                if (!obj.activeInHierarchy)
                {
                    obj.SetActive(true);
                    return obj;
                }
            }
        }

        // if there are no objects available, create another one
        foreach (PooledObject objectPool in pooledObjects)
        {
            // find object pool with matching tag
            if (objectPool.tag == tag && objectPool.shouldExpand)
            {
                // create new object
                GameObject obj = Instantiate(objectPool.obj);
                obj.SetActive(true);

                // add it to the corresponding list in the dicionary 
                objectPoolDictionary[tag].Add(obj);
                return obj;
            }
        }

        return null;
    }

    // function to change active objects into another object
    public void ChangeToObject(string currentObject, string newObject)
    {
        if (objectPoolDictionary.ContainsKey(currentObject) && objectPoolDictionary.ContainsKey(newObject))
        { 
            foreach (GameObject obj in objectPoolDictionary[currentObject])
            {
                // loop through each active object of current object list
                if (obj.activeInHierarchy)
                {
                    GameObject newObj = GetPooledObject(newObject);
                    obj.SetActive(false);
                    Vector2 position = obj.transform.position;
                    newObj.transform.position = position;
                }
            }
        }
    }

}
